package repo.tests;

import static org.junit.Assert.*;
import model.Address;
import model.Worker;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import database.MockDb;
import database.UnitOfWork;
import database.managers.IWorkerManager;
import database.managers.MockWorkerManager;
import database.unitOfWork.IUnitOfWork;

public class RepoWorkerTest {

	private MockDb db;
	private IUnitOfWork uow;
	
	@BeforeClass
    public static void setUpBeforeClass() throws Exception {
            
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }
    
	@Before
	public void setUp() throws Exception{
		db = new MockDb();
		Worker worker = new Worker();
		Address address = new Address();
		address.setCity("Warszawa");
		address.setStreet("Mickiewicza");
		address.setPostCode("22-222");
		
		worker.setNrWorker(1);
		worker.setFirstNameWorker("Jan");
		worker.setLastNameWorker("Kowalski");
		worker.setAddress(address);
		worker.setId(1);
		worker.setPhoneWorker("555-666-777");
		
		db.save(worker);
		
		uow = new UnitOfWork();
	}

	@After
    public void tearDown() throws Exception {
            db.getAllItems().clear();
    }

	/*@Test
	public void TestWorkerMock() {
		IWorkerManager rw = new MockWorkerManager(uow,db);
		Worker worker = rw.get(1);
		Worker worker2 = rw.get(1);
		
		assertNotNull("Nie udalo sie wyciagnac adresu",worker);
		assertTrue("Problem z imieniem", worker.getFirstNameWorker().equals("Jan"));
		assertEquals("Problem z pobraniem nazwiska",worker.getLastNameWorker(),"Kowalski");
		assertNotSame("obiekty wskazuja na ta sama przestrzen w pamieci",worker,worker2);
	}*/

}
