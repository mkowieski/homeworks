package repo;

import java.util.List;

import model.Specialization;
import model.EntityBase;
import database.MockDb;

public class RepoSpecialization implements IRepository<Specialization>{


	MockDb db;
	public RepoSpecialization(MockDb db){
		this.db=db;
	}
	@Override
	public Specialization get(int id) {
		return (Specialization)db.get(id);
	}

	@Override
	public List<Specialization> getAll() {
		return db.getItemsByType(Specialization.class);
	}

	@Override
	public void save(EntityBase E) {
		db.save(E);
	}

	@Override
	public void delete(EntityBase E) {
		db.delete(E);
	}

	@Override
	public void update(EntityBase E) {
		
	}
	@Override
	public String toString() {
		return "RepoSpecialization [db=" + db + "]";
	}
	
}
