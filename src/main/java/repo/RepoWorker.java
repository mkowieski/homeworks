package repo;

import java.util.List;

import model.Worker;
import model.EntityBase;
import database.MockDb;

public class RepoWorker implements IRepository<Worker>{


	MockDb db;
	public RepoWorker(MockDb db){
		this.db=db;
	}
	@Override
	public Worker get(int id) {
		return (Worker)db.get(id);
	}

	@Override
	public List<Worker> getAll() {
		return db.getItemsByType(Worker.class);
	}

	@Override
	public void save(EntityBase E) {
		db.save(E);
	}

	@Override
	public void delete(EntityBase E) {
		db.delete(E);
	}

	@Override
	public void update(EntityBase E) {
		
	}
	@Override
	public String toString() {
		return "RepoWorker [db=" + db + "]";
	}
	
}
