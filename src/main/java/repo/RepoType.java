package repo;

import java.util.List;

import model.Type;
import model.EntityBase;
import database.MockDb;

public class RepoType implements IRepository<Type>{


	MockDb db;
	public RepoType(MockDb db){
		this.db=db;
	}
	@Override
	public Type get(int id) {
		return (Type)db.get(id);
	}

	@Override
	public List<Type> getAll() {
		return db.getItemsByType(Type.class);
	}

	@Override
	public void save(EntityBase E) {
		db.save(E);
	}

	@Override
	public void delete(EntityBase E) {
		db.delete(E);
	}

	@Override
	public void update(EntityBase E) {
		
	}
	@Override
	public String toString() {
		return "RepoType [db=" + db + "]";
	}
	
}
