package repo;

import java.util.List;

import model.Bulding;
import model.EntityBase;
import database.MockDb;

public class RepoBulding implements IRepository<Bulding>{


	MockDb db;
	public RepoBulding(MockDb db){
		this.db=db;
	}
	@Override
	public Bulding get(int id) {
		return (Bulding)db.get(id);
	}

	@Override
	public List<Bulding> getAll() {
		return db.getItemsByType(Bulding.class);
	}

	@Override
	public void save(EntityBase E) {
		db.save(E);
	}

	@Override
	public void delete(EntityBase E) {
		db.delete(E);
	}

	@Override
	public void update(EntityBase E) {
		
	}
	@Override
	public String toString() {
		return "RepoBulding [db=" + db + "]";
	}
	
}
