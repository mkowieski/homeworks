package repo;

import java.util.List;

import model.Branch;
import model.EntityBase;
import database.MockDb;

public class RepoBranch implements IRepository<Branch>{


	MockDb db;
	public RepoBranch(MockDb db){
		this.db=db;
	}
	@Override
	public Branch get(int id) {
		return (Branch)db.get(id);
	}

	@Override
	public List<Branch> getAll() {
		return db.getItemsByType(Branch.class);
	}

	@Override
	public void save(EntityBase E) {
		db.save(E);
	}

	@Override
	public void delete(EntityBase E) {
		db.delete(E);
	}

	@Override
	public void update(EntityBase E) {
		
	}
	@Override
	public String toString() {
		return "RepoBranch [db=" + db + "]";
	}
	
}
