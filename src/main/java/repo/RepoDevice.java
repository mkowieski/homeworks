package repo;

import java.util.List;

import model.Device;
import model.EntityBase;
import database.MockDb;

public class RepoDevice implements IRepository<Device>{


	MockDb db;
	public RepoDevice(MockDb db){
		this.db=db;
	}
	@Override
	public Device get(int id) {
		return (Device)db.get(id);
	}

	@Override
	public List<Device> getAll() {
		return db.getItemsByType(Device.class);
	}

	@Override
	public void save(EntityBase E) {
		db.save(E);
	}

	@Override
	public void delete(EntityBase E) {
		db.delete(E);
	}

	@Override
	public void update(EntityBase E) {
		
	}
	@Override
	public String toString() {
		return "RepoDevice [db=" + db + "]";
	}
	
}
