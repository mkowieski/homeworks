package repo;

import java.util.List;

import model.Room;
import model.EntityBase;
import database.MockDb;

public class RepoRoom implements IRepository<Room>{


	MockDb db;
	public RepoRoom(MockDb db){
		this.db=db;
	}
	@Override
	public Room get(int id) {
		return (Room)db.get(id);
	}

	@Override
	public List<Room> getAll() {
		return db.getItemsByType(Room.class);
	}

	@Override
	public void save(EntityBase E) {
		db.save(E);
	}

	@Override
	public void delete(EntityBase E) {
		db.delete(E);
	}

	@Override
	public void update(EntityBase E) {
		
	}
	@Override
	public String toString() {
		return "RepoRoom [db=" + db + "]";
	}
	
}
