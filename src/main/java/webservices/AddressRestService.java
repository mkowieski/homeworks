package webservices;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import model.*;
import database.managers.*;
import dto.*;

@Path("address")
@Stateless
public class AddressRestService {
        
        @Inject
        private IAddressManager mgr;
        
        @GET
        @Path("/giveaddress/{id}")
        @Produces("application/json")
        public AddressSummaryDto giveAddress(@PathParam("id") int id)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Worker> builder = new WorkerBuilder();
                //IWorkerManager mgr = new PSQLWorkerManager(uow,builder,null);
        		Address a = mgr.get(id);
        		AddressSummaryDto result = new AddressSummaryDto();
        		result.setStreet(a.getStreet());
        		result.setPostCode(a.getPostCode());
        		result.setCity(a.getCity());
                return result;
                
        }
        
        public AddressSummaryDto saveAddress(AddressSummaryDto request)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Worker> builder = new WorkerBuilder();
                //IWorkerManager mgr = new PSQLWorkerManager(uow,builder,null);
        		Address a = new Address();
        		a.setStreet(request.getStreet());
        		a.setPostCode(request.getPostCode());
        		a.setCity(request.getCity());
                mgr.save(a);
                //uow.commit();
                mgr.saveChanges();
                AddressSummaryDto result = new AddressSummaryDto();
                result.setStreet(a.getStreet());
                result.setPostCode(a.getPostCode());
                result.setCity(a.getCity());
                return result;
                
        }
}