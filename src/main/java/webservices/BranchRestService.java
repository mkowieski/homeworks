package webservices;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import model.*;
import database.managers.*;
import dto.*;

@Path("branch")
@Stateless
public class BranchRestService {
        
        @Inject
        private IBranchManager mgr;
        
        @GET
        @Path("/givebranch/{id}")
        @Produces("application/json")
        public BranchSummaryDto giveBranch(@PathParam("id") int id)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Worker> builder = new WorkerBuilder();
                //IWorkerManager mgr = new PSQLWorkerManager(uow,builder,null);
        		Branch b = mgr.get(id);
        		BranchSummaryDto result = new BranchSummaryDto();
        		result.setNameBranch(b.getNameBranch());
                return result;
                
        }
        
        public BranchSummaryDto saveBranch(BranchSummaryDto request)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Worker> builder = new WorkerBuilder();
                //IWorkerManager mgr = new PSQLWorkerManager(uow,builder,null);
        		Branch b = new Branch();
        		b.setNameBranch(request.getNameBranch());
                mgr.save(b);
                //uow.commit();
                mgr.saveChanges();
                BranchSummaryDto result = new BranchSummaryDto();
                result.setNameBranch(b.getNameBranch());
                return result;
                
        }
}