package webservices;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import model.*;
import database.managers.*;
import dto.*;

@Path("specialization")
@Stateless
public class SpecializationRestService {
        
        @Inject
        private ISpecializationManager mgr;
        
        @GET
        @Path("/givespecialization/{id}")
        @Produces("application/json")
        public SpecializationSummaryDto giveSpecialization(@PathParam("id") int id)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Worker> builder = new WorkerBuilder();
                //IWorkerManager mgr = new PSQLWorkerManager(uow,builder,null);
                Specialization s = mgr.get(id);
                SpecializationSummaryDto result = new SpecializationSummaryDto();
                result.setNameSpecialization(s.getNameSpecialization());
                return result;
                
        }
        
        public SpecializationSummaryDto saveSpecialization(SpecializationSummaryDto request)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Worker> builder = new WorkerBuilder();
                //IWorkerManager mgr = new PSQLWorkerManager(uow,builder,null);
        		Specialization s = new Specialization();
                s.setNameSpecialization(request.getNameSpecialization());
                mgr.save(s);
                //uow.commit();
                mgr.saveChanges();
                SpecializationSummaryDto result = new SpecializationSummaryDto();
                result.setNameSpecialization(s.getNameSpecialization());
                
                return result;
                
        }
}