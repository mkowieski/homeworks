package webservices;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import model.Address;
import model.Worker;
import database.managers.IWorkerManager;
import dto.WorkerDto;
import dto.WorkerSummaryDto;

@Path("worker")
@Stateless
public class WorkerRestService {
        
        @Inject
        private IWorkerManager mgr;
        
        @GET
        @Path("/giveworker/{id}")
        @Produces("application/json")
        public WorkerDto giveWorker(@PathParam("id") int id)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Worker> builder = new WorkerBuilder();
                //IWorkerManager mgr = new PSQLWorkerManager(uow,builder,null);
                Worker w = mgr.get(id);
                Address a = new Address();
                WorkerDto result = new WorkerDto();
                result.setId(id);
				result.setFirstNameWorker(w.getFirstNameWorker());
				result.setLastNameWorker(w.getLastNameWorker());
				result.setPhoneWorker(w.getPhoneWorker());
				a.setStreet(w.getAddress().getStreet());
				a.setPostCode(w.getAddress().getPostCode());
				a.setCity(w.getAddress().getCity());
                //result.setAddress(a);
                return result;
                
        }
        
        public WorkerSummaryDto saveWorker(WorkerDto request)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Worker> builder = new WorkerBuilder();
                //IWorkerManager mgr = new PSQLWorkerManager(uow,builder,null);
                Worker w = new Worker();
                Address a = new Address();
                a.setStreet(a.getStreet());
                a.setPostCode(a.getPostCode());
                a.setCity(a.getCity());
                w.setFirstNameWorker(request.getFirstNameWorker());
                w.setLastNameWorker(request.getLastNameWorker());
                w.setPhoneWorker(request.getPhoneWorker());
                mgr.save(w);
                //uow.commit();
                mgr.saveChanges();
                WorkerSummaryDto result = new WorkerSummaryDto();
                result.setFirstNameWorker(w.getFirstNameWorker());
                result.setLastNameWorker(w.getLastNameWorker());
                result.setPhoneWorker(w.getPhoneWorker());
               //result.setAddress(a);
                
                return result;
                
        }
}