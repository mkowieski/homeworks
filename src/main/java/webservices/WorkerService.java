package webservices;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;
import model.Address;
import model.Worker;
import database.managers.IWorkerManager;
import dto.*;

@WebService
public class WorkerService {

		@Inject
		private IWorkerManager mgr;
		
        @WebMethod
        public WorkerDto giveWorker(int request)
        {
                Worker w = mgr.get(request);
                WorkerDto result = new WorkerDto();
                Address a = new Address();
                
                a.setStreet(a.getStreet());
                a.setPostCode(a.getPostCode());
                a.setCity(a.getCity());
                result.setFirstNameWorker(w.getFirstNameWorker());
                result.setLastNameWorker(w.getLastNameWorker());
                result.setPhoneWorker(w.getPhoneWorker());
              //  result.setAddress(a);
                
                return result; 
        }
        
        @WebMethod
        public WorkerSummaryDto saveWorker(WorkerDto request)
        {
                Worker w = new Worker();
                Address a = new Address();
                a.setStreet(a.getStreet());
                a.setPostCode(a.getPostCode());
                a.setCity(a.getCity());
                w.setFirstNameWorker(w.getFirstNameWorker());
                w.setLastNameWorker(w.getLastNameWorker());
                w.setPhoneWorker(w.getPhoneWorker());
                w.setAddress(a);
                mgr.save(w);
                mgr.saveChanges();
                WorkerSummaryDto result = new WorkerSummaryDto();
                result.setFirstNameWorker(w.getFirstNameWorker());
                result.setLastNameWorker(w.getLastNameWorker());
                result.setPhoneWorker(w.getPhoneWorker());
                //result.setAddress(a);
                return result;
                
        }
}
