package webservices;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import model.*;
import database.managers.ITypeManager;
import dto.TypeSummaryDto;

@Path("type")
@Stateless
public class TypeRestService {
        
        @Inject
        private ITypeManager mgr;
        
        @GET
        @Path("/givetype/{id}")
        @Produces("application/json")
        public TypeSummaryDto giveType(@PathParam("id") int id)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Worker> builder = new WorkerBuilder();
                //IWorkerManager mgr = new PSQLWorkerManager(uow,builder,null);
                Type t = mgr.get(id);
                TypeSummaryDto result = new TypeSummaryDto();
                result.setCategory(t.getCategory());
                return result;
                
        }
        
        public TypeSummaryDto saveType(TypeSummaryDto request)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Worker> builder = new WorkerBuilder();
                //IWorkerManager mgr = new PSQLWorkerManager(uow,builder,null);
                Type t = new Type();
                t.setCategory(request.getCategory());
                mgr.save(t);
                //uow.commit();
                mgr.saveChanges();
                TypeSummaryDto result = new TypeSummaryDto();
                result.setCategory(t.getCategory());
                
                return result;
                
        }
}