package webservices;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import model.*;
import database.managers.*;
import dto.*;

@Path("bulding")
@Stateless
public class BuldingRestService {
        
        @Inject
        private IBuldingManager mgr;
        
        @GET
        @Path("/givebulding/{id}")
        @Produces("application/json")
        public BuldingSummaryDto giveBulding(@PathParam("id") int id)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Worker> builder = new WorkerBuilder();
                //IWorkerManager mgr = new PSQLWorkerManager(uow,builder,null);
                Bulding b = mgr.get(id);
                Address a = new Address();
                BuldingSummaryDto result = new BuldingSummaryDto();
                a.setStreet(b.getAddressBulding().getStreet());
				a.setPostCode(b.getAddressBulding().getPostCode());
				a.setCity(b.getAddressBulding().getCity());
				result.setAddressBulding(a);
                return result;
                
        }
        
        public BuldingSummaryDto saveBulding(BuldingSummaryDto request)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Worker> builder = new WorkerBuilder();
                //IWorkerManager mgr = new PSQLWorkerManager(uow,builder,null);
        		Bulding b = new Bulding();
        		Address a = new Address();
                a.setStreet(a.getStreet());
                a.setPostCode(a.getPostCode());
                a.setCity(a.getCity());
        		b.setAddressBulding(a);
                mgr.save(b);
                //uow.commit();
                mgr.saveChanges();
                BuldingSummaryDto result = new BuldingSummaryDto();
                result.setAddressBulding(a);
                return result;
                
        }
}