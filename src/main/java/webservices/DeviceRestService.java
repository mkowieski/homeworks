package webservices;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import model.*;
import database.managers.*;
import dto.*;

@Path("device")
@Stateless
public class DeviceRestService {
        
        @Inject
        private IDeviceManager mgr;
        
        @GET
        @Path("/givedevice/{id}")
        @Produces("application/json")
        public DeviceSummaryDto giveDevice(@PathParam("id") int id)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Worker> builder = new WorkerBuilder();
                //IWorkerManager mgr = new PSQLWorkerManager(uow,builder,null);
                Device d = mgr.get(id);
                DeviceSummaryDto result = new DeviceSummaryDto();
                result.setDescriptionDevice(d.getDescriptionDevice());
                result.setBrand(d.getBrand());
                return result;
                
        }
        
        public DeviceSummaryDto saveDevice(DeviceSummaryDto request)
        {
                //UnitOfWork uow = new UnitOfWork();
                //IEntityBuilder<Worker> builder = new WorkerBuilder();
                //IWorkerManager mgr = new PSQLWorkerManager(uow,builder,null);
        		Device d = new Device();
        		d.setDescriptionDevice(request.getDescriptionDevice());
        		d.setBrand(request.getBrand());
                mgr.save(d);
                //uow.commit();
                mgr.saveChanges();
                DeviceSummaryDto result = new DeviceSummaryDto();
                result.setDescriptionDevice(d.getDescriptionDevice());
                
                return result;
                
        }
}