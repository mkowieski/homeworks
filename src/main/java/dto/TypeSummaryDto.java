package dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TypeSummaryDto {
	
	public String Category;
	
	public String getCategory() {
		return Category;
	}
	
	public void setCategory(String category) {
		Category = category;
	}

}
