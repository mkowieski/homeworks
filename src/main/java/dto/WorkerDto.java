package dto;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class WorkerDto extends WorkerSummaryDto{

		private List<AddressSummaryDto> addresses;

		public List<AddressSummaryDto> getAddresses() {
			return addresses;
        }
		
		public void setAddresses(List<AddressSummaryDto> address) {
			this.addresses = address;
        }
        
        
}
