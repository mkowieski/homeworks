package dto;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SpecializationSummaryDto {
	
	public String NameSpecialization;
	
	public String getNameSpecialization() {
		return NameSpecialization;
	}
	public void setNameSpecialization(String nameSpecialization) {
		NameSpecialization = nameSpecialization;
	}

}
