package dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RoomSummaryDto {
	
	public String DescriptionRoom;
	
	public String getDescriptionRoom() {
		return DescriptionRoom;
	}
	public void setDescriptionRoom(String descriptionRoom) {
		DescriptionRoom = descriptionRoom;
	}
}
