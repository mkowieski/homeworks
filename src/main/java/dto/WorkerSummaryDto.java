package dto;

import javax.xml.bind.annotation.XmlRootElement;

import model.Address;
@XmlRootElement
public class WorkerSummaryDto {
	public int Id;
	public String FirstNameWorker;
	public String LastNameWorker;
	public String PhoneWorker;
	public AddressSummaryDto Address;
	
	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getFirstNameWorker() {
		return FirstNameWorker;
	}

	public void setFirstNameWorker(String firstNameWorker) {
		FirstNameWorker = firstNameWorker;
	}

	public String getLastNameWorker() {
		return LastNameWorker;
	}

	public void setLastNameWorker(String lastNameWorker) {
		LastNameWorker = lastNameWorker;
	}

	public String getPhoneWorker() {
		return PhoneWorker;
	}

	public void setPhoneWorker(String phoneWorker) {
		PhoneWorker = phoneWorker;
	}

	public AddressSummaryDto getAddress() {
		return Address;
	}

	public void setAddress(AddressSummaryDto address) {
		Address = address;
	}
}
