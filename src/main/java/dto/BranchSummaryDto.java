package dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BranchSummaryDto {
	public String NameBranch;
	
	public String getNameBranch() {
		return NameBranch;
	}
	
	public void setNameBranch(String nameBranch) {
		NameBranch = nameBranch;
	}
}
