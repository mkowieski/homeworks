package dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DeviceSummaryDto {
	public String DescriptionDevice;
	public String Brand;
	
	public String getDescriptionDevice() {
		return DescriptionDevice;
	}
	public void setDescriptionDevice(String descriptionDevice) {
		DescriptionDevice = descriptionDevice;
	}
	public String getBrand() {
		return Brand;
	}
	public void setBrand(String brand) {
		Brand = brand;
	}
}
