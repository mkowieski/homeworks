package dto;

import javax.xml.bind.annotation.XmlRootElement;

import model.Address;
@XmlRootElement
public class BuldingSummaryDto {

	public Address AddressBulding;

	public Address getAddressBulding() {
		return AddressBulding;
	}

	public void setAddressBulding(Address addressBulding) {
		AddressBulding = addressBulding;
	}
}
