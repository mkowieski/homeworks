package model;

import model.EntityBase;

public class Address  extends EntityBase{
	public String PostCode;
	public String City;
	public String Street;

	public void setPostCode(String postCode) {
		PostCode = postCode;
	}

	public void setCity(String city) {
		City = city;
	}

	public void setStreet(String street) {
		Street = street;
	}

	public String getCity() {
		return City;
	}

	public String getPostCode() {
		return PostCode;
	}
	
	public String getStreet() {
		return Street;
	}

	@Override
	public String toString() {
		return Street + ", " + PostCode + ", " + City;
	}
	
	
	
	
}
