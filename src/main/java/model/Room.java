package model;

import model.EntityBase;

public class Room  extends EntityBase{
	public int NrRoom;
	public String DescriptionRoom;
	
	public int getNrRoom() {
		return NrRoom;
	}
	public void setNrRoom(int nrRoom) {
		NrRoom = nrRoom;
	}
	public String getDescriptionRoom() {
		return DescriptionRoom;
	}
	public void setDescriptionRoom(String descriptionRoom) {
		DescriptionRoom = descriptionRoom;
	}
	
	@Override
	public String toString() {
		return "Room [NrRoom=" + NrRoom + ", DescriptionRoom="
				+ DescriptionRoom + "]";
	}
	
}
