package model;

import model.EntityBase;

public class Type  extends EntityBase{
	public int NrType;
	public String Category;
	
	public int getNrType() {
		return NrType;
	}
	
	public void setNrType(int nrType) {
		NrType = nrType;
	}
	
	public String getCategory() {
		return Category;
	}
	
	public void setCategory(String category) {
		Category = category;
	}
	
	@Override
	public String toString() {
		return "Type [NrType=" + NrType + ", Category=" + Category + "]";
	}
	
	

}
