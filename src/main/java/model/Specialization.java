package model;

import model.EntityBase;

public class Specialization  extends EntityBase{
	public int NrSpecialization;
	public String NameSpecialization;
	
	public int getNrSpecialization() {
		return NrSpecialization;
	}
	public void setNrSpecialization(int nrSpecialization) {
		NrSpecialization = nrSpecialization;
	}
	public String getNameSpecialization() {
		return NameSpecialization;
	}
	public void setNameSpecialization(String nameSpecialization) {
		NameSpecialization = nameSpecialization;
	}
	
	@Override
	public String toString() {
		return "Specialization [NrSpecialization=" + NrSpecialization
				+ ", NameSpecialization=" + NameSpecialization + "]";
	}
	
	
}
