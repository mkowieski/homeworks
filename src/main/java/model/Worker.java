package model;

import model.EntityBase;

public class Worker extends EntityBase{
	
	public int NrWorker;
	public String FirstNameWorker;
	public String LastNameWorker;
	public String PhoneWorker;
	public Address Address;
	
	public int getNrWorker() {
		return NrWorker;
	}

	public void setNrWorker(int nrWorker) {
		NrWorker = nrWorker;
	}

	public String getFirstNameWorker() {
		return FirstNameWorker;
	}

	public void setFirstNameWorker(String firstNameWorker) {
		FirstNameWorker = firstNameWorker;
	}

	public String getLastNameWorker() {
		return LastNameWorker;
	}

	public void setLastNameWorker(String lastNameWorker) {
		LastNameWorker = lastNameWorker;
	}

	public String getPhoneWorker() {
		return PhoneWorker;
	}

	public void setPhoneWorker(String phoneWorker) {
		PhoneWorker = phoneWorker;
	}

	public Address getAddress() {
		return Address;
	}

	public void setAddress(Address address) {
		Address = address;
	}

	@Override
	public String toString() {
		return "Worker [NrWorker=" + NrWorker
				+ ", FirstNameWorker=" + FirstNameWorker
				+ ", LastNameWorker=" + LastNameWorker
				+ ", PhoneWorker=" + PhoneWorker + ", Address=" + Address
				+ "]";
	}
	
	
}
