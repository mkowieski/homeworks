package model;

import model.EntityBase;

public class Branch  extends EntityBase{
	public int NrBranch;
	public String NameBranch;
	
	public int getNrBranch() {
		return NrBranch;
	}
	public void setNrBranch(int nrBranch) {
		NrBranch = nrBranch;
	}
	public String getNameBranch() {
		return NameBranch;
	}
	public void setNameBranch(String nameBranch) {
		NameBranch = nameBranch;
	}
	
	@Override
	public String toString() {
		return "Branch [NrBranch=" + NrBranch + ", NameBranch=" + NameBranch
				+ "]";
	}
	
}
