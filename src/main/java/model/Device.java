package model;

import model.EntityBase;

public class Device extends EntityBase{
	public int NrDevice;
	public String DescriptionDevice;
	public String Brand;
	
	public int getNrDevice() {
		return NrDevice;
	}
	public void setNrDevice(int nrDevice) {
		NrDevice = nrDevice;
	}
	public String getDescriptionDevice() {
		return DescriptionDevice;
	}
	public void setDescriptionDevice(String descriptionDevice) {
		DescriptionDevice = descriptionDevice;
	}
	public String getBrand() {
		return Brand;
	}
	public void setBrand(String brand) {
		Brand = brand;
	}
	
	@Override
	public String toString() {
		return "Device [NrDevice=" + NrDevice + ", DescriptionDevice="
				+ DescriptionDevice + ", Brand=" + Brand + "]";
	}
	
	
}
