package model;

import model.EntityBase;

public class Bulding  extends EntityBase{
	public int NrBulding;
	public Address AddressBulding;
	
	public int getNrBulding() {
		return NrBulding;
	}

	public void setNrBulding(int nrBulding) {
		NrBulding = nrBulding;
	}

	public Address getAddressBulding() {
		return AddressBulding;
	}

	public void setAddressBulding(Address addressBulding) {
		AddressBulding = addressBulding;
	}

	@Override
	public String toString() {
		return "Bulding [NrBulding=" + NrBulding + ", AddressBulding="
				+ AddressBulding + "]";
	}
}
