package database.unitOfWork;

import model.EntityBase;

public interface IUnitOfWork {

	public void registerAdd(EntityBase ent, IUnitOfWorkRepository repo);
	public void registerDeleted(EntityBase ent, IUnitOfWorkRepository repo);
	public void registerUpdated(EntityBase ent, IUnitOfWorkRepository repo);
	public void commit();
	
}
