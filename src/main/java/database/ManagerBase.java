package database;

import java.util.List;

import database.unitOfWork.IUnitOfWork;
import database.unitOfWork.IUnitOfWorkRepository;
import model.EntityBase;

public abstract class ManagerBase<E extends EntityBase> implements IManager<E>,IUnitOfWorkRepository {

	protected IUnitOfWork uow;
	

	public ManagerBase(IUnitOfWork uow) {
		super();
		this.uow = uow;
	}

	@Override
	public void save(E obj) {
		uow.registerAdd(obj, this);
		
	}

	@Override
	public void delete(E obj) {
		uow.registerDeleted(obj, this);
		
	}
	
	@Override
	public void update(E obj)
	{
		uow.registerUpdated(obj, this);
	}
	
	public final void saveChanges()
	{
		uow.commit();
	}
	
	@Override
	public abstract E get(int id);

	@Override
	public abstract List<E> getAll(PageInfo request);
	
	@Override
	public abstract void persistAdd(EntityBase ent);

	@Override
	public abstract void persistDeleted(EntityBase ent);

	@Override
	public abstract void persistUpdated(EntityBase ent);
	
}
