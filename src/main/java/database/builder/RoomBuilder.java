package database.builder;

import java.sql.ResultSet;
import javax.enterprise.context.RequestScoped;
import model.Room;

@RequestScoped
public class RoomBuilder implements IEntityBuilder<Room>{
	
	@Override
    public Room build(ResultSet rs) {
            Room result = null;
            try{
                   result = new Room();
                   result.setId(rs.getInt("id"));
                   result.setDescriptionRoom(rs.getString("description"));
            }
            catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            return result;
    }
}
