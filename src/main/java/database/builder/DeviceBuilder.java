package database.builder;

import java.sql.ResultSet;
import javax.enterprise.context.RequestScoped;
import model.Device;

@RequestScoped
public class DeviceBuilder implements IEntityBuilder<Device>{
	
	@Override
    public Device build(ResultSet rs) {
		Device result = null;
            try{
                    result = new Device();
                    result.setId(rs.getInt("id"));
                    result.setDescriptionDevice(rs.getString("description"));
                    result.setBrand(rs.getString("brand"));
            }
            catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            return result;
    }
}
