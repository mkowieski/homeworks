package database.builder;

import java.sql.ResultSet;
import javax.enterprise.context.RequestScoped;
import model.Address;
import model.Worker;

@RequestScoped
public class WorkerBuilder implements IEntityBuilder<Worker>{

        @Override
        public Worker build(ResultSet rs) {
                Worker result = null;
                try{
                        Address a = new Address();
                        a.setStreet(rs.getString("street"));
        				a.setPostCode(rs.getString("postcode"));
        				a.setCity(rs.getString("city"));
                        result = new Worker();
                        result.setId(rs.getInt("id"));
                        result.setFirstNameWorker(rs.getString("firstname"));
                        result.setLastNameWorker(rs.getString("lastname"));
                        result.setPhoneWorker(rs.getString("phone"));
                        result.setAddress(a);
                }
                catch(Exception ex)
                {
                        ex.printStackTrace();
                }
                return result;
        }

}