package database.builder;

import java.sql.ResultSet;

import javax.enterprise.context.RequestScoped;

import model.Specialization;

@RequestScoped
public class SpecializationBuilder implements IEntityBuilder<Specialization>{
	
	@Override
    public Specialization build(ResultSet rs) {
		Specialization result = null;
            try{
            		result = new Specialization();
            		result.setId(rs.getInt("id"));
            		result.setNameSpecialization(rs.getString("name"));
            }
            catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            return result;
    }
}
