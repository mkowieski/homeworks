package database.builder;

import java.sql.ResultSet;

import javax.enterprise.context.RequestScoped;

import model.Address;
import model.Bulding;

@RequestScoped
public class BuldingBuilder implements IEntityBuilder<Bulding>{
	
	@Override
    public Bulding build(ResultSet rs) {
		Bulding result = null;
            try{
	            	Address a = new Address();
	                a.setStreet(rs.getString("street"));
					a.setPostCode(rs.getString("postcode"));
					a.setCity(rs.getString("city"));
                    result = new Bulding();
                    result.setId(rs.getInt("id"));
                    result.setAddressBulding(a);
            }
            catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            return result;
    }
}
