package database.builder;

import java.sql.ResultSet;
import javax.enterprise.context.RequestScoped;
import model.Type;

@RequestScoped
public class TypeBuilder implements IEntityBuilder<Type>{

	@Override
    public Type build(ResultSet rs) {
            Type result = null;
            try{
            		result = new Type();
            		result.setId(rs.getInt("id"));
            		result.setCategory(rs.getString("category"));
            }
            catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            return result;
    }
}
