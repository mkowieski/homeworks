package database.builder;

import java.sql.ResultSet;
import javax.enterprise.context.RequestScoped;
import model.Branch;

@RequestScoped
public class BranchBuilder implements IEntityBuilder<Branch>{
	
	@Override
    public Branch build(ResultSet rs) {
		Branch result = null;
            try{
                    result = new Branch();
                    result.setId(rs.getInt("id"));
                    result.setNameBranch(rs.getString("name"));
            }
            catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            return result;
    }
}
