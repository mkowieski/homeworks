package database.builder;

import java.sql.ResultSet;

import javax.enterprise.context.RequestScoped;

import model.Address;

@RequestScoped
public class AddressBuilder implements IEntityBuilder<Address> {

	@Override
	public Address build(ResultSet rs) {
		Address result = null;
        try{
        		result = new Address();
        		result.setId(rs.getInt("id"));
        		result.setStreet(rs.getString("street"));
        		result.setPostCode(rs.getString("postcode"));
				result.setCity(rs.getString("city"));
        }
        catch(Exception ex)
        {
                ex.printStackTrace();
        }
        return result;
	}

}
