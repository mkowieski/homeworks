package database;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;

import database.unitOfWork.IUnitOfWork;
import database.unitOfWork.IUnitOfWorkRepository;
import model.EntityBase;
@RequestScoped
public class UnitOfWork implements IUnitOfWork{

	
	private Map<EntityBase, IUnitOfWorkRepository> added;
	private Map<EntityBase, IUnitOfWorkRepository> deleted;
	private Map<EntityBase, IUnitOfWorkRepository> changed;
	
	public UnitOfWork() {
		added = new HashMap<EntityBase, IUnitOfWorkRepository>();
		deleted = new HashMap<EntityBase, IUnitOfWorkRepository>();
		changed = new HashMap<EntityBase, IUnitOfWorkRepository>();

	}

	@Override
	public void registerAdd(EntityBase ent, IUnitOfWorkRepository repo) {
		added.put(ent, repo);
		
	}

	@Override
	public void registerDeleted(EntityBase ent, IUnitOfWorkRepository repo) {
		deleted.put(ent, repo);
		
	}

	@Override
	public void registerUpdated(EntityBase ent, IUnitOfWorkRepository repo) {
		changed.put(ent, repo);
		
	}
	
	@Override
	public void commit() {
		for(EntityBase ent:added.keySet())
		{
			added.get(ent).persistAdd(ent);
		}
		for(EntityBase ent:changed.keySet())
		{
			changed.get(ent).persistUpdated(ent);
		}
		for(EntityBase ent:deleted.keySet())
		{
			deleted.get(ent).persistDeleted(ent);
		}
		added.clear();
		changed.clear();
		deleted.clear();
	}

}
