package database.managers;

import java.util.List;

import database.ManagerBase;
import database.MockDb;
import database.PageInfo;
import database.unitOfWork.IUnitOfWork;
import model.Device;
import model.EntityBase;

public class MockDeviceManager extends ManagerBase<Device> implements IDeviceManager{
	
	private MockDb db;
	
	public MockDeviceManager(IUnitOfWork uow, MockDb db){
		this(uow);
		this.db=db;
	}

	public MockDeviceManager(IUnitOfWork uow) {
		super(uow);
	}

	@Override
	public Device get(int id) {
		return (Device)db.get(id);
	}

	@Override
	public List<Device> getAll(PageInfo request) {
		return db.getItemsByType(Device.class);
	}

	@Override
	public String getDescriptionDevice(Device d) {
		return d.getDescriptionDevice();
	}

	@Override
	public String getBrand(Device d) {
		return d.getBrand();
	}

	@Override
	public void persistAdd(EntityBase ent) {
		db.save(ent);
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		db.delete(ent);
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		// TODO Auto-generated method stub
		
	}

}
