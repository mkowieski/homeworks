package database.managers;

import model.Address;
import model.Worker;
import database.IManager;

public interface IAddressManager extends IManager<Address>{
	public void setAddressWorker(Worker w);
}
