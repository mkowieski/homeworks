package database.managers;

import java.util.List;

import model.EntityBase;
import model.Worker;
import database.ManagerBase;
import database.MockDb;
import database.PageInfo;
import database.managers.IWorkerManager;
import database.unitOfWork.IUnitOfWork;

public class MockWorkerManager extends ManagerBase<Worker> implements IWorkerManager {

	private MockDb db;
	
	public MockWorkerManager(IUnitOfWork uow, MockDb db){
		this(uow);
		this.db=db;
	}
	
	public MockWorkerManager(IUnitOfWork uow) {
		super(uow);
	}

	@Override
	public void setAddress(Worker w) {

	}

	@Override
	public Worker get(int id) {
		return (Worker)db.get(id);
	}

	@Override
	public List<Worker> getAll(PageInfo request) {
		return db.getItemsByType(Worker.class);
	}

	@Override
	public void persistAdd(EntityBase ent) {
		db.save(ent);
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		db.delete(ent);
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		// TODO Auto-generated method stub
		
	}

}
