package database.managers;

import model.Room;
import database.IManager;

public interface IRoomManager extends IManager<Room>{
	public String getDescriptionRoom(Room r);
}
