package database.managers;

import model.Device;
import database.IManager;

public interface IDeviceManager extends IManager<Device>{
	public String getDescriptionDevice(Device d);
	public String getBrand(Device d);
}
