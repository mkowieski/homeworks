package database.managers;

import model.*;
import database.IManager;

public interface IWorkerManager extends IManager<Worker>{
	
	public void setAddress(Worker w);

}
