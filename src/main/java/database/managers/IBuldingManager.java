package database.managers;

import java.util.List;

import model.Address;
import model.Bulding;
import database.IManager;

public interface IBuldingManager extends IManager<Bulding>{
	public List<Address> getAddressBulding(Bulding b);
}
