package database.managers;

import java.util.List;

import model.Address;
import model.Bulding;
import model.EntityBase;
import database.ManagerBase;
import database.MockDb;
import database.PageInfo;
import database.unitOfWork.IUnitOfWork;

public class MockBuldingManager extends ManagerBase<Bulding> implements IBuldingManager{

	private MockDb db;
	
	public MockBuldingManager(IUnitOfWork uow, MockDb db){
		this(uow);
		this.db=db;
	}
	
	public MockBuldingManager(IUnitOfWork uow) {
		super(uow);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void save(Bulding obj) {
		// TODO Auto-generated method stub
	}

	@Override
	public void delete(Bulding obj) {
		// TODO Auto-generated method stub
	}

	@Override
	public List<Address> getAddressBulding(Bulding b) {
		return  (List<Address>) b.getAddressBulding();
	}

	@Override
	public Bulding get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Bulding> getAll(PageInfo request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void persistAdd(EntityBase ent) {
		db.save(ent);
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		db.delete(ent);
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		// TODO Auto-generated method stub
		
	}
}
