package database.managers;

import java.util.List;

import model.EntityBase;
import model.Type;
import database.ManagerBase;
import database.MockDb;
import database.PageInfo;
import database.unitOfWork.IUnitOfWork;

public class MockTypeManager extends ManagerBase<Type> implements ITypeManager{
	
	private MockDb db;

	public MockTypeManager(IUnitOfWork uow) {
		super(uow);
	}

	@Override
	public String getCategory(Type t) {
		return t.getCategory();
	}

	@Override
	public Type get(int id) {
		return (Type)db.get(id);
	}

	@Override
	public List<Type> getAll(PageInfo request) {
		return db.getItemsByType(Type.class);
	}

	@Override
	public void persistAdd(EntityBase ent) {
		db.save(ent);
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		db.delete(ent);
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		// TODO Auto-generated method stub
		
	}
	

}
