package database.managers;

import model.Branch;
import database.IManager;

public interface IBranchManager extends IManager<Branch>{
	public String getNameBranch(Branch b);
}
