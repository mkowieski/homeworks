package database.managers;

import java.util.List;

import database.ManagerBase;
import database.MockDb;
import database.PageInfo;
import database.unitOfWork.IUnitOfWork;
import model.EntityBase;
import model.Room;

public class MockRoomManager extends ManagerBase<Room> implements IRoomManager{
	
	private MockDb db;
	
	public MockRoomManager(IUnitOfWork uow, MockDb db){
		this(uow);
		this.db=db;
	}
	
	public MockRoomManager(IUnitOfWork uow) {
		super(uow);
	}

	@Override
	public Room get(int id) {
		return (Room)db.get(id);
	}

	@Override
	public List<Room> getAll(PageInfo request) {
		return db.getItemsByType(Room.class);
	}

	@Override
	public String getDescriptionRoom(Room r) {
		return r.getDescriptionRoom();
	}

	@Override
	public void persistAdd(EntityBase ent) {
		db.save(ent);
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		db.delete(ent);
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		// TODO Auto-generated method stub
		
	}

}
