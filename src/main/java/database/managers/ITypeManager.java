package database.managers;

import model.Type;
import database.IManager;

public interface ITypeManager extends IManager<Type>{
	public String getCategory(Type t);
}
