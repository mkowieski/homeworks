package database.managers.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import model.Address;
import model.EntityBase;
import model.Worker;
import database.ManagerBase;
import database.PageInfo;
import database.builder.IEntityBuilder;
import database.managers.IAddressManager;
import database.unitOfWork.IUnitOfWork;

@RequestScoped
public class PSQLAddressManager extends ManagerBase<Address> implements IAddressManager{

	public PSQLAddressManager(IUnitOfWork uow) {
        super(uow);
        
	}
	private Connection connection;
	private Statement statement;
	private String login="postgres";
	private String password="123";
	private String url="jdbc:postgresql://localhost:5432/postgres";
	
	private PreparedStatement getAddressForWorker;
    private PreparedStatement getAddressById;
    private PreparedStatement getAllAddress;
    private PreparedStatement update;
    private PreparedStatement delete;
    private PreparedStatement insert;
    
    private IEntityBuilder<Address> builder;
    
    private String createTableAddress = "CREATE TABLE address "
						    		 + "(id serial, "
						    		 + "street varchar(16), "
						    		 + "postcode varchar(16), "
						    		 + "city varchar(32));";
	
    @Inject
	public PSQLAddressManager(IUnitOfWork uow, IEntityBuilder<Address> builder) {
		super(uow);
		this.builder=builder;
		
		try{
			connection = DriverManager.getConnection(url,login,password);
	        statement =connection.createStatement();
	        
	        ResultSet rs = connection.getMetaData()
	                        .getTables(null,null,null,null);
	        
	        boolean exist = false;
	        while(rs.next())
	        {
	                if("address".equalsIgnoreCase(rs.getString("TABLE_NAME")))
	                {
	                        exist=true;
	                        break;
	                }
	        }
	        if(!exist)
            {
                    statement.executeUpdate(createTableAddress);
            }
		        getAddressById = connection.prepareStatement(""
	                    + "SELECT * FROM address WHERE id=?");
			    getAllAddress = connection.prepareStatement(
			                    "SELECT * FROM address");
			    insert = connection.prepareStatement(""
			                    + "INSERT INTO address(street,postcode,city)"
			                    + "VALUES (?,?,?)");
			    delete = connection.prepareStatement(""
			                    + "DELETE FROM address WHERE id=?");
			    update = connection.prepareStatement("UPDATE address SET "
			                    + "street=? "
			                    + "postcode=? "
			                    + "city=? "
			                    + "WHERE id=?");
			    getAddressForWorker = connection.prepareStatement(
			                    "SELECT * FROM address WHERE WorkerId=?");
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void setAddressWorker(Worker w) {
		try
        {
                getAddressForWorker.setInt(1, w.getId());
                ResultSet rs = getAddressForWorker.executeQuery();
                while(rs.next())
                {
                        Address a = builder.build(rs);
                        //TO NIE DZIA�A ----------------------------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                        //w.getAddress().add(a);
                        
                }
        }
        catch(Exception ex){
                ex.printStackTrace();
        }
		
	}

	@Override
	public Address get(int id) {
		Address a = null;
        try
        {
                getAddressById.setInt(1, id);
                ResultSet rs = getAddressById.executeQuery();
                while(rs.next())
                {
                        a = builder.build(rs);
                }
        }
        catch(Exception ex)
        {
                ex.printStackTrace();
        }
        
        return a;
	}

	@Override
	public List<Address> getAll(PageInfo request) {
		List<Address> addreses = new ArrayList<Address>();
        
        try
        {
        	ResultSet rs = getAllAddress.executeQuery();
        	while(rs.next()){
        	Address result = new Address();
        	result.setId(rs.getInt("id"));
        	result.setStreet(rs.getString("street"));
        	result.setPostCode(rs.getString("postcode"));
        	result.setCity(rs.getString("city"));
        	addreses.add(result);
        	}
        }catch(Exception ex)
        {
                ex.printStackTrace();
        }
        
        return addreses;
	}

	@Override
	public void persistAdd(EntityBase ent) {
		Address a = (Address)ent;
		try{
			insert.setString(1, a.getStreet());
			insert.setString(2, a.getPostCode());
			insert.setString(3, a.getCity());
			insert.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		Address a = (Address)ent;
		try{
			delete.setInt(1, a.getId());
			delete.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		Address w = (Address)ent;
		try{
			update.setString(1, w.getStreet());
			update.setString(2, w.getPostCode());
			update.setString(3, w.getCity());
			update.setInt(4, w.getId());
			update.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}
	
}
