package database.managers.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import model.Address;
import model.Bulding;
import model.EntityBase;
import model.Type;
import model.Worker;
import database.ManagerBase;
import database.PageInfo;
import database.builder.IEntityBuilder;
import database.managers.IBuldingManager;
import database.managers.ITypeManager;
import database.unitOfWork.IUnitOfWork;

@RequestScoped
public class PSQLBuldingManager extends ManagerBase<Bulding> implements IBuldingManager{

	public PSQLBuldingManager(IUnitOfWork uow) {
        super(uow);
	}
	
	private IBuldingManager buldingMgr;
	private IEntityBuilder<Bulding> builder;
	
	private Connection connection;
	private Statement statement;
	private String login="postgres";
	private String password="123";
	private String url="jdbc:postgresql://localhost:5432/postgres";
	
	private PreparedStatement getBuldingById;
	private PreparedStatement getAllBuldings;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	private PreparedStatement getAddressBulding;
	
	private String createTableBulding = "CREATE TABLE bulding (id serial, street varchar(32), postcode varchar(16), city varchar(32));";
	
	@Inject
	public PSQLBuldingManager(IUnitOfWork uow, 
            IEntityBuilder<Bulding> builder,
            IBuldingManager buldingMgr){
		super(uow);
		
		this.builder=builder;
        this.buldingMgr=buldingMgr;
		
		try {
			connection = DriverManager.getConnection(url,login,password);
			statement = connection.createStatement();
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			
			boolean exist = false;
			while(rs.next()){
				if("bulding".equalsIgnoreCase(rs.getString("TABLE_NAME"))){
					exist=true;
					break;
				}
			}
			
			if(!exist){
				statement.executeUpdate(createTableBulding);
			}
			
			getBuldingById = connection.prepareStatement("SELECT * FROM bulding WHERE id=?;");
			getAllBuldings = connection.prepareStatement("SELECT * FROM bulding;");
			getAddressBulding = connection.prepareStatement("SELECT street,postcode,city FROM bulding WHERE id=?;");
			insert = connection.prepareStatement("INSERT INTO bulding(street,postcode,city) VALUES (?,?,?);");
			delete = connection.prepareStatement("DELETE FROM bulding WHERE id=?;");
			update = connection.prepareStatement("UPDATE bulding SET street=? postcode=? city=? WHERE id=?;");
			
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	@Override
	public List<Address> getAddressBulding(Bulding b) {
		List<Address> address = new ArrayList<Address>();
		try{
			getAddressBulding.setInt(1, b.getId());
			ResultSet rs = getAddressBulding.executeQuery();
			while(rs.next()){
				Address a = new Address();
				a.setId(rs.getInt("id"));
				a.setStreet(rs.getString("street"));
				a.setPostCode(rs.getString("postcode"));
				a.setCity(rs.getString("city"));
				address.add(a);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return address;
	}

	@Override
	public Bulding get(int id) {
		Bulding result = null;
		try{
			getBuldingById.setInt(1, id);
			ResultSet rs = getBuldingById.executeQuery();
			while(rs.next()){
				result = builder.build(rs);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Bulding> getAll(PageInfo request) {
		List<Bulding> buldings = new ArrayList<Bulding>();
		try{
			int n = request.getPageIndex()*request.getPageSize();
			getAllBuldings.setInt(1, n);
			getAllBuldings.setInt(2, request.getPageSize());
			ResultSet rs = getAllBuldings.executeQuery();
			while(rs.next()){
				Bulding result = builder.build(rs);
				buldings.add(result);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return buldings;
	}

	@Override
	public void persistAdd(EntityBase ent) {
		Bulding b = (Bulding)ent;
		try{
			insert.setString(1, b.getAddressBulding().getStreet());
			insert.setString(2, b.getAddressBulding().getPostCode());
			insert.setString(3, b.getAddressBulding().getCity());
			insert.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		Bulding w = (Bulding)ent;
		try{
			delete.setInt(1, w.getId());
			delete.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		Bulding b = (Bulding)ent;
		try{
			update.setString(1, b.getAddressBulding().getStreet());
			update.setString(2, b.getAddressBulding().getPostCode());
			update.setString(3, b.getAddressBulding().getCity());
			update.setInt(4, b.getId());
			update.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

}
