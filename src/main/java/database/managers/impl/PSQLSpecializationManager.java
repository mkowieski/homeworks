package database.managers.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import model.Branch;
import model.Device;
import model.EntityBase;
import model.Specialization;
import model.Type;
import model.Worker;
import database.ManagerBase;
import database.PageInfo;
import database.builder.IEntityBuilder;
import database.managers.ISpecializationManager;
import database.managers.ITypeManager;
import database.unitOfWork.IUnitOfWork;

@RequestScoped
public class PSQLSpecializationManager extends ManagerBase<Specialization> implements ISpecializationManager{

	public PSQLSpecializationManager(IUnitOfWork uow) {
        super(uow);
	}
	
	private ISpecializationManager specializationMgr;
	private IEntityBuilder<Specialization> builder;
	
	private Connection connection;
	private Statement statement;
	private String login="postgres";
	private String password="123";
	private String url="jdbc:postgresql://localhost:5432/postgres";
	
	private PreparedStatement getSpecializationById;
	private PreparedStatement getAllSpecializations;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	private PreparedStatement getNameSpecialization;
	
	private String createTableSpecialization = "CREATE TABLE specialization (id serial, name varchar(64));";
	
	@Inject
	public PSQLSpecializationManager(IUnitOfWork uow, 
            IEntityBuilder<Specialization> builder,
            ISpecializationManager specializationMgr){
		super(uow);
		
		this.builder=builder;
        this.specializationMgr=specializationMgr;
		
		try {
			connection = DriverManager.getConnection(url,login,password);
			statement = connection.createStatement();
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			
			boolean exist = false;
			while(rs.next()){
				if("specialization".equalsIgnoreCase(rs.getString("TABLE_NAME"))){
					exist=true;
					break;
				}
			}
			
			if(!exist){
				statement.executeUpdate(createTableSpecialization);
			}
			
			getSpecializationById = connection.prepareStatement("SELECT * FROM specialization WHERE id=?;");
			getAllSpecializations = connection.prepareStatement("SELECT * FROM specialization;");
			getNameSpecialization = connection.prepareStatement("SELECT name FROM specialization WHERE id=?;");
			insert = connection.prepareStatement("INSERT INTO specialization(name) "
					+ "VALUES (?);");
			delete = connection.prepareStatement("DELETE FROM specialization WHERE id=?;");
			update = connection.prepareStatement("UPDATE specialization SET name=? WHERE id=?;");
			
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	@Override
	public String getNameSpecialization(Specialization s) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Specialization get(int id) {
		Specialization result = null;
		try{
			getSpecializationById.setInt(1, id);
			ResultSet rs = getSpecializationById.executeQuery();
			while(rs.next()){
				result = builder.build(rs);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Specialization> getAll(PageInfo request) {
		List<Specialization> specializations = new ArrayList<Specialization>();
		try{
			int n = request.getPageIndex()*request.getPageSize();
			getAllSpecializations.setInt(1, n);
			getAllSpecializations.setInt(2, request.getPageSize());
			ResultSet rs = getAllSpecializations.executeQuery();
			while(rs.next()){
				Specialization result = builder.build(rs);
				specializations.add(result);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return specializations;
	}

	@Override
	public void persistAdd(EntityBase ent) {
		Specialization s = (Specialization)ent;
		try{
			insert.setString(1, s.getNameSpecialization());
			insert.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		Specialization w = (Specialization)ent;
		try{
			delete.setInt(1, w.getId());
			delete.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		Specialization d = (Specialization)ent;
		try{
			update.setString(1, d.getNameSpecialization());
			update.setInt(2, d.getId());
			update.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

}
