package database.managers.impl;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import model.EntityBase;
import model.Worker;
import database.ManagerBase;
import database.PageInfo;
import database.builder.IEntityBuilder;
import database.managers.IAddressManager;
import database.managers.IWorkerManager;
import database.unitOfWork.IUnitOfWork;

@RequestScoped
public class PSQLWorkerManager extends ManagerBase<Worker> implements IWorkerManager{
	
	public PSQLWorkerManager(IUnitOfWork uow) {
        super(uow);
	}
	
	private IAddressManager addressMgr;
	private Connection connection;
	private Statement statement;
	private String login="postgres";
	private String password="123";
	private String url="jdbc:postgresql://localhost:5432/postgres";
	
	private PreparedStatement getWorkerById;
	private PreparedStatement getAllWorkers;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	private IEntityBuilder<Worker> builder;
	
	
	private String createTableWorker = "CREATE TABLE worker (id serial, firstname varchar(16), lastname varchar(32), phone varchar(16), street varchar(32), "
			+ "postcode varchar(16), city varchar(32));";
	@Inject
	 public PSQLWorkerManager(IUnitOfWork uow, 
             IEntityBuilder<Worker> builder,
             IAddressManager addressMgr){
		super(uow);

		this.builder=builder;
        this.addressMgr=addressMgr;
        
        try {
       
            connection = DriverManager.getConnection(url,login,password);
            statement =connection.createStatement();
            
            ResultSet rs = connection.getMetaData()
                            .getTables(null,null,null,null);
            
            boolean exist = false;
            while(rs.next())
            {
                    if("worker".equalsIgnoreCase(rs.getString("TABLE_NAME")))
                    {
                            exist=true;
                            break;
                    }
            }
            
            if(!exist)
            {
            	statement.executeUpdate(createTableWorker);
            }
			
			getWorkerById = connection.prepareStatement("SELECT * FROM worker WHERE id=?;");
			getAllWorkers = connection.prepareStatement("SELECT * FROM worker;");
			insert = connection.prepareStatement("INSERT INTO worker(firstname,lastname,phone,street,postcode,city) "
					+ "VALUES (?,?,?,?,?,?);");
			delete = connection.prepareStatement("DELETE FROM worker WHERE id=?;");
			update = connection.prepareStatement("UPDATE worker SET firstname=? lastname=? phone=? street=? postcode=? city=? WHERE id=?;");
			
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	@Override
	public void setAddress(Worker w) {
		addressMgr.setAddressWorker(w);
	}

	@Override
	public Worker get(int id) {
		Worker result = null;
		try{
			getWorkerById.setInt(1, id);
			ResultSet rs = getWorkerById.executeQuery();
			while(rs.next()){
				result = builder.build(rs);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Worker> getAll(PageInfo request) {
		List<Worker> books = new ArrayList<Worker>();
		
			try{
			int n = request.getPageIndex()*request.getPageSize();
			getAllWorkers.setInt(1, n);
			getAllWorkers.setInt(2, request.getPageSize());
			ResultSet rs = getAllWorkers.executeQuery();
			while(rs.next())
			{
				Worker result = builder.build(rs);
				
				books.add(result);
			}
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return books;
	}

	@Override
	public void persistAdd(EntityBase ent) {
		Worker w = (Worker)ent;
		try{
			insert.setString(1, w.getFirstNameWorker());
			insert.setString(2, w.getLastNameWorker());
			insert.setString(3, w.getPhoneWorker());
			insert.setString(4, w.getAddress().getStreet());
			insert.setString(5, w.getAddress().getPostCode());
			insert.setString(6, w.getAddress().getCity());
			insert.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		Worker w = (Worker)ent;
		try{
			delete.setInt(1, w.getId());
			delete.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		Worker w = (Worker)ent;
		try{
			update.setString(1, w.getFirstNameWorker());
			update.setString(2, w.getLastNameWorker());
			update.setString(3, w.getPhoneWorker());
			update.setString(4, w.getAddress().getStreet());
			update.setString(5, w.getAddress().getPostCode());
			update.setString(6, w.getAddress().getCity());
			update.setInt(7, w.getId());
			update.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}

}
