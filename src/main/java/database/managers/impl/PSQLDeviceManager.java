package database.managers.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import model.Address;
import model.Device;
import model.EntityBase;
import model.Room;
import model.Type;
import model.Worker;
import database.ManagerBase;
import database.PageInfo;
import database.builder.IEntityBuilder;
import database.managers.IDeviceManager;
import database.managers.IRoomManager;
import database.managers.ITypeManager;
import database.unitOfWork.IUnitOfWork;

@RequestScoped
public class PSQLDeviceManager extends ManagerBase<Device> implements IDeviceManager{

	public PSQLDeviceManager(IUnitOfWork uow) {
        super(uow);
	}
	
	private IDeviceManager deviceMgr;
	private IEntityBuilder<Device> builder;
	
	private Connection connection;
	private Statement statement;
	private String login="postgres";
	private String password="123";
	private String url="jdbc:postgresql://localhost:5432/postgres";
	
	private PreparedStatement getDeviceById;
	private PreparedStatement getAllDevices;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	private PreparedStatement getDescriptionDevice;
	private PreparedStatement getBrand;
	
	private String createTableDevice = "CREATE TABLE device (id serial, description varchar(255), brand varchar(64));";
	
	@Inject
	public PSQLDeviceManager(IUnitOfWork uow, 
            IEntityBuilder<Device> builder,
            IDeviceManager deviceMgr){
		super(uow);
		
		this.builder=builder;
        this.deviceMgr=deviceMgr;
		
		try {
			connection = DriverManager.getConnection(url,login,password);
			statement = connection.createStatement();
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			
			boolean exist = false;
			while(rs.next()){
				if("device".equalsIgnoreCase(rs.getString("TABLE_NAME"))){
					exist=true;
					break;
				}
			}
			
			if(!exist){
				statement.executeUpdate(createTableDevice);
			}
			
			getDeviceById = connection.prepareStatement("SELECT * FROM device WHERE id=?;");
			getAllDevices = connection.prepareStatement("SELECT * FROM device;");
			getDescriptionDevice = connection.prepareStatement("SELECT description FROM device WHERE id=?;");
			getBrand = connection.prepareStatement("SELECT brand FROM device WHERE id=?;");
			insert = connection.prepareStatement("INSERT INTO device(description,brand) "
					+ "VALUES (?,?);");
			delete = connection.prepareStatement("DELETE FROM device WHERE id=?;");
			update = connection.prepareStatement("UPDATE device SET description=? brand=? WHERE id=?;");
			
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	@Override
	public String getDescriptionDevice(Device d) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getBrand(Device d) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Device get(int id) {
		Device result = null;
		try{
			getDeviceById.setInt(1, id);
			ResultSet rs = getDeviceById.executeQuery();
			while(rs.next()){
				result = builder.build(rs);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Device> getAll(PageInfo request) {
		List<Device> devices = new ArrayList<Device>();
		try{
			int n = request.getPageIndex()*request.getPageSize();
			getAllDevices.setInt(1, n);
			getAllDevices.setInt(2, request.getPageSize());
			ResultSet rs = getAllDevices.executeQuery();
			while(rs.next()){
				Device result = builder.build(rs);
				devices.add(result);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return devices;
	}

	@Override
	public void persistAdd(EntityBase ent) {
		Device d = (Device)ent;
		try{
			insert.setString(1, d.getDescriptionDevice());
			insert.setString(2, d.getBrand());
			insert.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		Device w = (Device)ent;
		try{
			delete.setInt(1, w.getId());
			delete.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		Device d = (Device)ent;
		try{
			update.setString(1, d.getDescriptionDevice());
			update.setString(2, d.getBrand());
			update.setInt(3, d.getId());
			update.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

}
