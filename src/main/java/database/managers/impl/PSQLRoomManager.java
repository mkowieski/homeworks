package database.managers.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import model.EntityBase;
import model.Room;
import model.Type;
import database.ManagerBase;
import database.PageInfo;
import database.builder.IEntityBuilder;
import database.managers.IRoomManager;
import database.managers.ITypeManager;
import database.unitOfWork.IUnitOfWork;

@RequestScoped
public class PSQLRoomManager extends ManagerBase<Room> implements IRoomManager{

	public PSQLRoomManager(IUnitOfWork uow) {
        super(uow);
	}
	
	private IRoomManager roomMgr;
	private IEntityBuilder<Room> builder;
	
	private Connection connection;
	private Statement statement;
	private String login="postgres";
	private String password="123";
	private String url="jdbc:postgresql://localhost:5432/postgres";
	
	private PreparedStatement getRoomById;
	private PreparedStatement getAllRooms;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	private PreparedStatement getDescriptionRoom;
	
	private String createTableRoom = "CREATE TABLE room (id serial, description varchar(255));";
	
	@Inject
	public PSQLRoomManager(IUnitOfWork uow, 
            IEntityBuilder<Room> builder,
            IRoomManager roomMgr){
		super(uow);
		
		this.builder=builder;
        this.roomMgr=roomMgr;
		
		try {
			connection = DriverManager.getConnection(url,login,password);
			statement = connection.createStatement();
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			
			boolean exist = false;
			while(rs.next()){
				if("room".equalsIgnoreCase(rs.getString("TABLE_NAME"))){
					exist=true;
					break;
				}
			}
			
			if(!exist){
				statement.executeUpdate(createTableRoom);
			}
			
			getRoomById = connection.prepareStatement("SELECT * FROM room WHERE id=?;");
			getAllRooms = connection.prepareStatement("SELECT * FROM room;");
			getDescriptionRoom = connection.prepareStatement("SELECT description FROM room;");
			insert = connection.prepareStatement("INSERT INTO room(description) VALUES (?);");
			delete = connection.prepareStatement("DELETE FROM room WHERE id=?;");
			update = connection.prepareStatement("UPDATE room SET street=? postcode=? city=? WHERE id=?;");
			
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	@Override
	public String getDescriptionRoom(Room r) {
		// TODO Auto-generated method stub
		try {
			ResultSet rs = getDescriptionRoom.executeQuery();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Room get(int id) {
		Room result = null;
		try{
			getRoomById.setInt(1, id);
			ResultSet rs = getRoomById.executeQuery();
			while(rs.next()){
				result = builder.build(rs);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Room> getAll(PageInfo request) {
		List<Room> rooms = new ArrayList<Room>();
		try{
			int n = request.getPageIndex()*request.getPageSize();
			getAllRooms.setInt(1, n);
			getAllRooms.setInt(2, request.getPageSize());
			ResultSet rs = getAllRooms.executeQuery();
			while(rs.next()){
				Room result = builder.build(rs);
				rooms.add(result);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return rooms;
	}

	@Override
	public void persistAdd(EntityBase ent) {
		Room w = (Room)ent;
		try{
			insert.setString(1, w.getDescriptionRoom());
			insert.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		Room w = (Room)ent;
		try{
			delete.setInt(1, w.getId());
			delete.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		Room w = (Room)ent;
		try{
			update.setString(1, w.getDescriptionRoom());
			update.setInt(2, w.getId());
			update.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

}
