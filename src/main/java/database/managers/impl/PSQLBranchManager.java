package database.managers.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import model.Address;
import model.Branch;
import model.Device;
import model.EntityBase;
import model.Type;
import model.Worker;
import database.ManagerBase;
import database.PageInfo;
import database.builder.IEntityBuilder;
import database.managers.IBranchManager;
import database.managers.ITypeManager;
import database.unitOfWork.IUnitOfWork;

@RequestScoped
public class PSQLBranchManager extends ManagerBase<Branch> implements IBranchManager{
	
	public PSQLBranchManager(IUnitOfWork uow) {
        super(uow);
	}
	
	private IBranchManager branchMgr;
	private IEntityBuilder<Branch> builder;
	
	private Connection connection;
	private Statement statement;
	private String login="postgres";
	private String password="123";
	private String url="jdbc:postgresql://localhost:5432/postgres";
	
	private PreparedStatement getBranchById;
	private PreparedStatement getAllBranches;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	private PreparedStatement getNameBranch;
	
	private String createTableBranch = "CREATE TABLE branch (id serial, name varchar(64));";
	
	@Inject
	public PSQLBranchManager(IUnitOfWork uow, 
            IEntityBuilder<Branch> builder,
            IBranchManager branchMgr){
		super(uow);
		
		this.builder=builder;
        this.branchMgr=branchMgr;
		
		try {
			connection = DriverManager.getConnection(url,login,password);
			statement = connection.createStatement();
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			
			boolean exist = false;
			while(rs.next()){
				if("branch".equalsIgnoreCase(rs.getString("TABLE_NAME"))){
					exist=true;
					break;
				}
			}
			
			if(!exist){
				statement.executeUpdate(createTableBranch);
			}
			
			getBranchById = connection.prepareStatement("SELECT * FROM branch WHERE id=?;");
			getAllBranches = connection.prepareStatement("SELECT * FROM branch;");
			getNameBranch = connection.prepareStatement("SELECT name FROM branch WHERE id=?;");
			insert = connection.prepareStatement("INSERT INTO branch(name) "
					+ "VALUES (?);");
			delete = connection.prepareStatement("DELETE FROM branch WHERE id=?;");
			update = connection.prepareStatement("UPDATE branch SET name=? WHERE id=?;");
			
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}
	
	@Override
	public String getNameBranch(Branch b) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Branch get(int id) {
		Branch result = null;
		try{
			getBranchById.setInt(1, id);
			ResultSet rs = getBranchById.executeQuery();
			while(rs.next()){
				result = builder.build(rs);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}
	@Override
	public List<Branch> getAll(PageInfo request) {
		List<Branch> branches = new ArrayList<Branch>();
		try{
			int n = request.getPageIndex()*request.getPageSize();
			getAllBranches.setInt(1, n);
			getAllBranches.setInt(2, request.getPageSize());
			ResultSet rs = getAllBranches.executeQuery();
			while(rs.next()){
				Branch result = builder.build(rs);
				branches.add(result);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return branches; 
	}
	
	@Override
	public void persistAdd(EntityBase ent) {
		Branch b = (Branch)ent;
		try{
			insert.setString(1, b.getNameBranch());
			insert.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void persistDeleted(EntityBase ent) {
		Branch b = (Branch)ent;
		try{
			delete.setInt(1, b.getId());
			delete.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void persistUpdated(EntityBase ent) {
		Branch b = (Branch)ent;
		try{
			update.setString(1, b.getNameBranch());
			update.setInt(2, b.getId());
			update.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
}
