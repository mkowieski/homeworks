package database.managers.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import model.EntityBase;
import model.Type;
import model.Worker;
import database.ManagerBase;
import database.PageInfo;
import database.builder.IEntityBuilder;
import database.managers.IAddressManager;
import database.managers.ITypeManager;
import database.unitOfWork.IUnitOfWork;

@RequestScoped
public class PSQLTypeManager extends ManagerBase<Type> implements ITypeManager{
	
	public PSQLTypeManager(IUnitOfWork uow) {
        super(uow);
	}
	
	private ITypeManager typeMgr;
	private IEntityBuilder<Type> builder;
	private Connection connection;
	private Statement statement;
	private String login="postgres";
	private String password="123";
	private String url="jdbc:postgresql://localhost:5432/postgres";
	
	private PreparedStatement getTypeById;
	private PreparedStatement getAllTypes;
	private PreparedStatement getCategory;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	
	private String createTableType = "CREATE TABLE type (id serial, category varchar(64));";
	
	@Inject
	public PSQLTypeManager(IUnitOfWork uow, 
            IEntityBuilder<Type> builder,
            ITypeManager typeMgr){
		super(uow);
		
		this.builder=builder;
        this.typeMgr=typeMgr;

		try {
			connection = DriverManager.getConnection(url,login,password);
			statement = connection.createStatement();
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			
			boolean exist = false;
			while(rs.next()){
				if("type".equalsIgnoreCase(rs.getString("TABLE_NAME"))){
					exist=true;
					break;
				}
			}
			
			if(!exist){
				statement.executeUpdate(createTableType);
			}
			
			getTypeById = connection.prepareStatement("SELECT * FROM type WHERE id=?;");
			getAllTypes = connection.prepareStatement("SELECT * FROM type;");
			getCategory = connection.prepareStatement("SELECT category FROM type WHERE id=?;");
			insert = connection.prepareStatement("INSERT INTO type(category) VALUES (?);");
			delete = connection.prepareStatement("DELETE FROM type WHERE id=?;");
			update = connection.prepareStatement("UPDATE type SET category=? WHERE id=?;");
			
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}
	
	@Override
	public String getCategory(Type t) {
		return null;
	}

	@Override
	public Type get(int id) {
		Type result = null;
		try{
			getTypeById.setInt(1, id);
			ResultSet rs = getTypeById.executeQuery();
			while(rs.next()){
				result = builder.build(rs);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Type> getAll(PageInfo request) {
		List<Type> types = new ArrayList<Type>();
		try{
			int n = request.getPageIndex()*request.getPageSize();
			getAllTypes.setInt(1, n);
			getAllTypes.setInt(2, request.getPageSize());
			ResultSet rs = getAllTypes.executeQuery();
			while(rs.next()){
				Type result = builder.build(rs);
				types.add(result);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return types;
	}

	@Override
	public void persistAdd(EntityBase ent) {
		Type t = (Type)ent;
		try{
			insert.setString(1, t.getCategory());
			insert.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		Type t = (Type)ent;
		try{
			delete.setInt(1, t.getId());
			delete.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		Type t = (Type)ent;
		try{
			update.setString(1, t.getCategory());
			update.setInt(2, t.getId());
			update.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}

}
