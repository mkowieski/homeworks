package database.managers.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import model.EntityBase;
import model.Worker;
import database.HibUnitOfWork;
import database.ManagerBase;
import database.PageInfo;
import database.managers.IWorkerManager;
import database.unitOfWork.IUnitOfWork;

public class HibWorkerManager extends ManagerBase<Worker> implements IWorkerManager {

	Session session;
	
	public HibWorkerManager(HibUnitOfWork uow) {
		super(uow);
		this.session=uow.getSession();
	}

	@Override
	public void setAddress(Worker w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Worker get(int id) {
		boolean openedNewSesson=false;
		if(!session.isOpen())
		{
			session = ((HibUnitOfWork)uow).getSession();
			openedNewSesson=true;
		}try{
		return (Worker)session.get(Worker.class, id);
		}finally
		{
			if(openedNewSesson)
				session.close();
		}
	}

	@Override
	public List<Worker> getAll(PageInfo request) {
		Query query = session.createQuery("from Workers");
        query.setFirstResult(request.getPageIndex()  * request.getPageSize());
        query.setMaxResults(request.getPageSize());
		return (List<Worker>)query.list();
	}

	@Override
	public void persistAdd(EntityBase ent) {
		Worker w = (Worker)ent;
		session.save(w);
		
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		session.delete((Worker)ent);
		
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		session.update((Worker)ent);
		
	}


}
