package database.managers;

import model.Specialization;
import database.IManager;

public interface ISpecializationManager extends IManager<Specialization>{
	public String getNameSpecialization(Specialization s);
}
