package database.managers;

import java.util.List;

import model.EntityBase;
import model.Specialization;
import database.ManagerBase;
import database.MockDb;
import database.PageInfo;
import database.unitOfWork.IUnitOfWork;

public class MockSpecializationManager extends ManagerBase<Specialization> implements ISpecializationManager{

	private MockDb db;
	
	public MockSpecializationManager(IUnitOfWork uow, MockDb db){
		this(uow);
		this.db=db;
	}
	
	public MockSpecializationManager(IUnitOfWork uow) {
		super(uow);
	}

	@Override
	public String getNameSpecialization(Specialization s) {
		return s.getNameSpecialization();
	}

	@Override
	public Specialization get(int id) {
		return (Specialization)db.get(id);
	}

	@Override
	public List<Specialization> getAll(PageInfo request) {
		return db.getItemsByType(Specialization.class);
	}

	@Override
	public void persistAdd(EntityBase ent) {
		db.save(ent);
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		db.delete(ent);
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		// TODO Auto-generated method stub
		
	}

}
