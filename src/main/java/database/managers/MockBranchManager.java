package database.managers;

import java.util.List;

import model.Branch;
import model.EntityBase;
import database.ManagerBase;
import database.MockDb;
import database.PageInfo;
import database.unitOfWork.IUnitOfWork;

public class MockBranchManager extends ManagerBase<Branch> implements IBranchManager{

	private MockDb db;
	
	public MockBranchManager(IUnitOfWork uow, MockDb db){
		this(uow);
		this.db=db;
	}
	
	public MockBranchManager(IUnitOfWork uow) {
		super(uow);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getNameBranch(Branch b) {
		return b.getNameBranch();
	}

	@Override
	public Branch get(int id) {
		return (Branch)db.get(id);
	}

	@Override
	public List<Branch> getAll(PageInfo request) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void persistAdd(EntityBase ent) {
		db.save(ent);
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		db.delete(ent);
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		// TODO Auto-generated method stub
		
	}

}
