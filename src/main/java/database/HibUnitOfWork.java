package database;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import model.EntityBase;
import database.unitOfWork.IUnitOfWork;
import database.unitOfWork.IUnitOfWorkRepository;

public class HibUnitOfWork implements IUnitOfWork{

	SessionFactory factory;
	Session session;
	private Map<EntityBase, IUnitOfWorkRepository> added;
	private Map<EntityBase, IUnitOfWorkRepository> deleted;
	private Map<EntityBase, IUnitOfWorkRepository> changed;
	
	
	public Session getSession() {
		if(session == null || !session.isOpen())
			session=factory.openSession();
		return session;
	}

	public HibUnitOfWork() {
		super();
		added = new HashMap<EntityBase, IUnitOfWorkRepository>();
		deleted = new HashMap<EntityBase, IUnitOfWorkRepository>();
		changed = new HashMap<EntityBase, IUnitOfWorkRepository>();
		factory = new Configuration().configure().buildSessionFactory();
	}

	@Override
	public void registerAdd(EntityBase ent, IUnitOfWorkRepository repo) {
		added.put(ent, repo);
		
	}

	@Override
	public void registerDeleted(EntityBase ent, IUnitOfWorkRepository repo) {
		deleted.put(ent, repo);
		
	}

	@Override
	public void registerUpdated(EntityBase ent, IUnitOfWorkRepository repo) {
		changed.put(ent, repo);
		
	}
	
	@Override
	public void commit() {
		try{
			getSession().beginTransaction();
			
			for(EntityBase ent:added.keySet())
			{
				added.get(ent).persistAdd(ent);
			}
			for(EntityBase ent:changed.keySet())
			{
				changed.get(ent).persistUpdated(ent);
			}
			for(EntityBase ent:deleted.keySet())
			{
				deleted.get(ent).persistDeleted(ent);
			}
			
			getSession().getTransaction().commit();
			added.clear();
			changed.clear();
			deleted.clear();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			if(getSession().isOpen())
			getSession().close();
		}
	}
}
